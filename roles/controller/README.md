`gurisitx.devops.controller` role
=========

Installs requirements for a devops environement in a computer used as ansible controller to develop, configure and deploy an Infrasructure as Code (IaC). You generally run this role on localhost, to update your computer packages and ansible collections and roles

Requirements
------------

In fact, the role suffers of a kid of chicken and egg syndrome, as you need a first version of ansible to run it, but it remains quite usefull to complete and update controllers configuration, and to overun some limitations of `ansible-galaxy` utility to switch among several branches with differents sets of roles and collection versions, whend developping roles for an IaC.

It may deserve to explore `ansible-pull` to develop a proces consistent version of the role. 

Role Variables
--------------

`gurisitx_min_ansible_ver` is the minimal version of ansible required for the IaC. Default value is 2.10

`gurisitx_requirements_file` is the ansible collections and roles required files. By default, it's as usual `requirements.yml`, relative, in the playbook's folder. 

`gurisitx_collections_path`: Path to use for collections' git clone or downloads. Default value tries to guess ansible default one: `{{ lookup('ansible.builtin.config', 'COLLECTIONS_PATHS') | first }}`

`gurisitx_roles_path`: Path to use for roles git clone and downloads. Default value tries to guess ansible default one: `{{ lookup('ansible.builtin.config', 'DEFAULT_ROLES_PATH') | first }}`


Dependencies
------------

No specific dependency.

Example Playbook
----------------

This role is generally applied on localhost, called by a very simple playbook:

```yaml
- name: Comprobar versión de Ansible
  hosts: localhost

  roles:
  - role: gurisitx.devops.controller

```

License
-------

GPLv3-or-later

Author Information
------------------

Daniel Viñar Ulriksen <daniel@uruguayos.fr>

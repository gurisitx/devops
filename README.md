# Ansible Collection - gurisitx.devops

This collection intends to group some tools for devops work in development, integration and deployment of collections and ansible IaCs.

Presently it contains one role: 
- [`gurisitx.devops.controller`](roles/controller), which configures some dependencies and ansible requirements file collections and roles, as git repos when possible, as static files if not. 

